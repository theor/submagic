from os.path import isfile, join, splitext
import argparse
import logging
import random
import sys,os,re

from colorama import init
from pprint import pprint as pp
from colorama import Fore, Back, Style

init()
#colors = [Fore.BLACK, Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.BLUE, Fore.MAGENTA, Fore.CYAN, Fore.WHITE, Fore.RESET]

title = "SUBMAGIC"
padding = ' '*15
print(padding + Fore.CYAN + ("*" * (len(title)+4)))
print(padding + "* %s *" % title)
print(padding + Fore.CYAN + ("*" * (len(title)+4)) + Fore.RESET)

parser = argparse.ArgumentParser(description='Submagic')
parser.add_argument('-n','--dry-run', help='do not modify files', action='store_true')
parser.add_argument('-v', '--verbose', help='extra verbosity level', action='store_true')
parser.add_argument('-s', '--sub-exts', metavar="EXT", nargs="*", default=['srt', 'sub'], help='subtitles extensions')
parser.add_argument('-e', '--vid-exts', metavar="EXT", nargs="*", default=['mp4', 'avi'], help='episodes extensions')
parser.add_argument('-r', '--episodes-regexes', metavar="R", nargs="*", default=['(\d\d?)x(\d\d)', 's(\d\d)e(\d\d)'], help='regexes to match season and episode')
parser.add_argument('dir', default=os.getcwd(), nargs="?")

args = parser.parse_args(sys.argv[1:])
for k,v in vars(args).iteritems():
	print('%s : %s' % (k.rjust(20), v))
print('')

logging.basicConfig(level=logging.INFO if not args.verbose else logging.DEBUG)

def index(filelist):
	'''
	Takes a list of filenames
	Returns a dict (season, episode) => filename
	'''
	subdict = dict()

	for sub in filelist:
		logging.debug("Sub %s", sub)
		for subre in args.episodes_regexes:
			# logging.debug("  re %s", subre)
			match = re.search(subre, sub.lower())
			if match: # this regex matches
				s,e = int(match.group(1)),int(match.group(2))
				logging.debug("S%02iE%02i", s,e)
				subdict[(s,e)] = sub
				break
		else:
			logging.info("  skipping sub, could not match")
	#pp(subdict)
	return subdict

action_colors = {
	'-':Fore.RESET,
	'M':Fore.RED,
	'S':Fore.MAGENTA
}

def action(a):
	return action_colors[a] + a + Fore.RESET

# Main program

files = [f for f in os.listdir(args.dir) if isfile(join(args.dir, f))]

# subtitles in folder
subfiles = [f for f in files if any(f.endswith(subext) for subext in args.sub_exts)]
if args.verbose:
	print("Subs:")
	pp(subfiles)
subs = index(subfiles)
# episodes in folder
movfiles = [f for f in files if any(f.endswith(movext) for movext in args.vid_exts)]
if args.verbose:
	print("Episodes:")
	pp(movfiles)
movs = index(movfiles)


if not any(movs):
	print("No episode with the following extension: %r" % args.vid_exts)
	sys.exit(0)

# loop on episodes, try to match a subtitle
for s,e in sorted(movs):
	movname, movext = splitext(movs[s,e])
	# matching sub
	if not (s,e) in subs:
		continue

	sub = subs[s,e]
	subname, subexts = splitext(sub)
	print("%s*     S%02iE%02i%s %s"%(Fore.CYAN, s, e, Fore.RESET, movname[:60] + (movname[60:]and '..')+movext))

	# different filename ?
	if movname != subname:
		newSubName = movname + subexts
		
		fullOldPath, fullNewPath = join(args.dir, sub), join(args.dir, newSubName)
		logging.debug(os.path.exists(fullOldPath) )

		if os.path.exists(fullNewPath): 
			print("  [%s] %s" % (action('S'), sub))
			print("    Skipped, file '%s' already exists" % newSubName)
			continue

		# need to rename the file
		print("  [%s] %s to %s" % (action('M'), sub, newSubName))
		if not args.dry_run:
			os.rename(fullOldPath, fullNewPath)
	else:
		print("  [%s] Nothing to do" % action('-'))


		