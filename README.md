# SUBMAGIC #

matches the subtitles in a folder to the tv episodes and rename them accordingly.

### USAGE ###

```
#!shell


usage: submagic.py [-h] [-n] [-v] [-s [EXT [EXT ...]]] [-e [EXT [EXT ...]]]
                   [-r [R [R ...]]]
                   [dir]

Submagic

positional arguments:
  dir


optional arguments:
  -h, --help            show this help message and exit
  -n, --dry-run         do not modify files (default: False)
  -v, --verbose         extra verbosity level (default: False)
  -s [EXT [EXT ...]], --sub-exts [EXT [EXT ...]]
                        subtitles extensions (default: ['srt', 'sub'])
  -e [EXT [EXT ...]], --vid-exts [EXT [EXT ...]]
                        episodes extensions (default: ['mp4', 'avi'])
  -r [R [R ...]], --episodes-regexes [R [R ...]]
                        regexes to match season and episode (default:
                        ['(\\d\\d?)x(\\d\\d)', 's(\\d\\d)e(\\d\\d)'])
```